-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 05, 2020 at 09:55 PM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `technical_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE `address` (
  `id` int(11) NOT NULL,
  `address` varchar(200) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_by` int(11) NOT NULL,
  `status` enum('active','inactive','deleted') NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`id`, `address`, `employee_id`, `created_at`, `created_by`, `status`) VALUES
(1, 'Bhopal', 5, '2020-09-05 17:16:18', 1, 'active'),
(2, 'Indore', 5, '2020-09-05 17:16:18', 1, 'active'),
(3, 'Bhopal', 7, '2020-09-05 19:31:37', 1, 'active'),
(4, 'Indore', 7, '2020-09-05 19:31:37', 1, 'active');

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` enum('active','inactive','deleted') NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `name`, `created_at`, `status`) VALUES
(2, 'Updated Company Cool', '2020-09-05 17:22:50', 'active'),
(3, 'First Auto Company', '2020-09-05 18:00:41', 'active'),
(4, 'New Company ', '2020-09-05 18:24:03', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(11) NOT NULL,
  `contact_no` varchar(15) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_by` int(11) NOT NULL,
  `status` enum('active','inactive','deleted') NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `contact_no`, `employee_id`, `created_at`, `created_by`, `status`) VALUES
(1, '7000129902', 5, '2020-09-05 17:16:18', 1, 'active'),
(2, '9893368545', 5, '2020-09-05 17:16:18', 1, 'active');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `company_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` enum('active','inactive','deleted') NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `name`, `company_id`, `created_at`, `status`) VALUES
(2, 'IT Department', 2, '2020-09-05 18:47:28', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `department_id` int(11) NOT NULL,
  `password_hash` varchar(500) NOT NULL,
  `password_reset_token` varchar(255) DEFAULT NULL,
  `verification_token` varchar(200) DEFAULT NULL,
  `auth_key` varchar(50) NOT NULL,
  `access_token` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` int(20) DEFAULT NULL,
  `status` enum('active','inactive','deleted') NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `department_id`, `password_hash`, `password_reset_token`, `verification_token`, `auth_key`, `access_token`, `created_at`, `updated_at`, `status`) VALUES
(1, 'admin', 1, '$2y$13$in1M1ThFV8FxbMp.obqtXuOcOBa5jSojE3utNP0nnUyFiyTkWkrCC', NULL, '', 'FU_b4cIcXwSLreAG8H3uwjKvkMulFlP4', 'JvP3PpYw3P7khMCTVMbw8eqfVbKCwDwJ', '2020-09-05 09:42:56', 1599304876, 'active'),
(2, 'jaswantjatav', 1, '$2y$13$/hhG3NJqtLzILFtBWHRoD..RYIFQte1cK9mDQg45Q31DYOSMeYfTO', 'fWqTbBkQ8e13AVTpVT8kPII5PdC6Iow3_1599325312', NULL, 'Xpdj0z4Aw2lmANIqSgv07uASH2_GdnDF', 'hKoz-6N6v3taUh6jmcmy1Jo-JvQj1wjK', '2020-09-05 17:01:52', NULL, 'active'),
(3, 'employee1', 1, '$2y$13$LvCnjz.gdvl2mBAkVnrOxOPFGu2yBsjQ8sTQ8muA.DPdsN9QXQnj.', 'gcBxqQE4EmIOHaEBB8BcZOVazsDZL_gS_1599325995', NULL, 'SI65lglxOHDyVRH587HwLC_Z4BvdEzAY', 'q_CvNEoCODqAgiXAANzO_iBGqYVtvSCD', '2020-09-05 17:13:15', NULL, 'active'),
(4, 'employee2', 1, '$2y$13$XAP1mj2B.z1ui3xlQeJXe.sIhKf3ytD1htwucs4qj61NbEWl8KoqO', 'r6d-xpgxEOL32OvCvOemIO8zLcZw0YIR_1599326139', NULL, 'ieS5QymzH-xsol8Xvf2QehE3mBvbMXrC', 'sh7PZxlg_0keIinIG3HR8uM17k7pNmK7', '2020-09-05 17:15:39', NULL, 'active'),
(5, 'employee3', 1, '$2y$13$uGrv.FCOUvCzBmZQigtGnOd0rtQAf7M4OS87Y3RQ7szTZqaoAECSS', 'sVpznTUOJ_zp0ywrAHZncaivp5Qw7Hx4_1599326178', NULL, 'TfUoNTLX4yCdKKhj-DqX9KB5pY_9P5jK', 'BZxeNyGKSEDm2sIzwkWCJB8WSwk0X8RB', '2020-09-05 17:16:18', NULL, 'active'),
(6, 'rajkumar', 1, '$2y$13$GeZgpjercBCYej4KHBkpNe0SHYTZd4CU43fh6WuZWAzShqFI/aoEm', '_EJQuE6UtX_I1uKgYwVep5bfNsz44RHU_1599331114', NULL, 'KO8w2PylTJjOQYpDVkeLmBf5d5Wf3V8-', 'R52TZugLdsJzGxw2ABud5kNkMk333mCh', '2020-09-05 18:38:34', NULL, 'active'),
(7, 'Dineshkumar', 2, '$2y$13$Gu67OUk7c59rWKq.xo2lcufvMsYQyt9Ii9pBmFHpSUeL/PE7VucjW', 'ZGuFvjvTDO-e5PLbgUwn-KchO_dEZm6n_1599334297', NULL, 'W879P6eJnZE02b2aqoewbvzSdwHvxZc9', 'd05auI0bZrK7aDZK8WS_Uff3GnN5qLqH', '2020-09-05 19:31:37', NULL, 'active');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_address` (`employee_id`);

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `contact_user` (`employee_id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `department_company` (`company_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `department_id` (`department_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `address`
--
ALTER TABLE `address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `address`
--
ALTER TABLE `address`
  ADD CONSTRAINT `user_address` FOREIGN KEY (`employee_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `contacts`
--
ALTER TABLE `contacts`
  ADD CONSTRAINT `contact_user` FOREIGN KEY (`employee_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `departments`
--
ALTER TABLE `departments`
  ADD CONSTRAINT `department_company` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
