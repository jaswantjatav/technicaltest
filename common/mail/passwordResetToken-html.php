<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
?>
<div style='width:100%;text-align: center;margin:0 auto;padding:0;'>
    <div class="password-reset" style='width:500px;font-size:20px;'>
        <p style='text-align: center;'>Hi <?= Html::encode($user->fname . ' ' . $user->lname) ?>,</p>
        <p><br/></p>
        <p style='text-align: center;'>We are sending you this email because you requested a password reset. Click on this link to create a new password</p>
        <p><br/></p>
        <p style='text-align: center;'><a href='<?= $resetLink; ?>' style='text-decoration: none;font-weight: bold;border-radius: 8px;padding:20px 60px;font-size:20px;background:#86c441;color:#fff;'>Set a new password</a></p>
        <p><br/></p>
        <p><br/></p>
        <p style='text-align: center;'>If you didn't request a password reset, you can ignore this email. Your password will not be changed.</p>
    </div>
</div>