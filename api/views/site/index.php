<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\Breadcrumbs;
use yii\helpers\ArrayHelper;

$this->title = 'Dashboard';
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1 class="cbreadcrum">
        Dashboard
        <small>Control panel</small>
    </h1 >
    <?=
    Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs']
                    : [],
    ])
    ?>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3><?= $custCount; ?></h3>

                    <p>Total Customers</p>
                </div>
                <div class="icon">
                    <i class="ion ion-person-add"></i>
                </div>

                <?php if (Yii::$app->user->identity->role == 'ADMIN') { ?>
                    <a href="<?= Url::toRoute(['customers/index']); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                <?php } else { ?>
                    <a href="<?= Url::toRoute(['customers/index?id=']) . Yii::$app->user->identity->ID; ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                <?php } ?>

            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3><?= $enquCount; ?></h3>


                    <p>Total Enquiry</p>
                </div>
                <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                </div>

                <?php if (Yii::$app->user->identity->role == 'ADMIN') { ?>
                    <a href="<?= Url::toRoute(['enquiries/index']); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                <?php } else { ?>
                    <a href="<?= Url::toRoute(['enquiries/index?id=']) . Yii::$app->dealer->identity->ID; ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                <?php } ?>

            </div>
        </div>
        <!-- ./col -->
        <?php if (Yii::$app->user->identity->role == 'ADMIN') { ?>
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3><?= $tractorCount; ?></h3>

                        <p>Total Tractors</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-android-cart"></i>
                    </div>

                    <a href="<?= Url::toRoute(['tractors/index']); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>

                </div>
            </div>
        <?php } ?>
        <!-- ./col -->
        <?php if (Yii::$app->user->identity->role == 'ADMIN') { ?>
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3><?= $dealersCount; ?></h3>

                        <p>Total Dealers</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
                    <a href="<?= Url::toRoute(['dealers/index']); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>

                </div>

            </div>

        
    <?php } ?>
        
    </div>
    <!-- ./col -->

    <!-- /.row -->

    <div class="row">
        <div class="col-lg-12">

            <h3>
                Latest Enquiries

            </h3>
        </div>
    </div>


    <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">

                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example2" class="table table-bordered table-hover">
                        <tbody>
                            <?php Pjax::begin(); ?>                            <?=
                            GridView::widget([
                                'dataProvider' => $dataProvider,
                                'columns' => [
                                    ['class' => 'yii\grid\SerialColumn'],
//                                'id',
                                'EnquiryModelNo',
//                                    
                                            'EnquiryName',
                                            'EnquiryMobileNo',
                                            'EnquiryEmail:email',
                                            // 'houseno',
                                            // 'street',
                                            // 'location',
                                            // 'zipcode',
                                            // 'city',
                                            // 'country',
                                            // 'state',
                                            // 'role',
                                            // 'auth_key',
                                            // 'access_token',
                                            // 'password_reset_token',
                                            // 'status',
                                            'EnquiryStatus',    
                                             [
                                        'attribute' => 'EnquiryCreated',
                                        'format' => 'raw',
                                        'value' => function ($model) {
                                            return date("d-M-Y H:i A", strtotime($model->EnquiryCreated));
                                        }
                                    ], 
                                            [
                                                'class' => 'yii\grid\ActionColumn',
                                                'template' => '{new_action1}{new_action2}',
                                                'buttons' => [
                                                    'new_action1' => function ($url, $model) {
                                                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>',
                                                                        Url::base() . "/enquiries/view?id=" . $model->EnquiryID,
                                                                        [
                                                                    'title' => Yii::t('app',
                                                                            'New Action1'),
                                                        ]);
                                                    }
                                                        ],
                                                    ],
                                                ],
                                            ]);
                                            ?>
                                            <?php Pjax::end(); ?>                 
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>