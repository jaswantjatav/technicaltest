<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * JvP3PpYw3P7khMCTVMbw8eqfVbKCwDwJ
 * &expand=addresses,contacts
 */

namespace api\controllers;

use Yii;
use yii\rest\ActiveController;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\HttpBearerAuth;
use backend\models\Employee;
use backend\models\Contact;
use backend\models\Address;

class EmployeeController extends ActiveController {

    public $modelClass = 'backend\models\Employee';

    public function actions() {
        
        $actions = parent::actions();
        // disable the "update" and "create" actions
        unset($actions['update'], $actions['create']);
        return $actions;
        
    }

    public function behaviors() {

        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                HttpBasicAuth::className(),
                HttpBearerAuth::className(),
                QueryParamAuth::className(),
            ],
        ];
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className()
        ];
        return $behaviors;
    }

    public function actionCreate() {

        $model = new Employee();
        $model->setPassword(Yii::$app->request->post('password'));
        $model->generateAuthKey();
        $model->generatePasswordResetToken();
        $model->generateAccessToken();
        $model->username = Yii::$app->request->post('username');
        $model->department_id = Yii::$app->request->post('department_id');

        if ($model->save()) {

            if (!empty(Yii::$app->request->post('contact'))) {
                $contacts = Yii::$app->request->post('contact');
                foreach ($contacts as $contactNo) {
                    $contactModel = new Contact();
                    $contactModel->contact_no = $contactNo;
                    $contactModel->created_by = Yii::$app->user->identity->id;
                    $contactModel->employee_id = $model->id;
                    $contactModel->save();
                }
            }
            if (!empty(Yii::$app->request->post('address'))) {
                $addresses = Yii::$app->request->post('address');
                foreach ($addresses as $address) {
                    $addressModel = new Address();
                    $addressModel->address = $address;
                    $addressModel->employee_id = $model->id;
                    $addressModel->created_by = Yii::$app->user->identity->id;
                    $addressModel->save();
                }
            }

            return [
                'status' => true,
                'data' => $model
            ];
        } else {
            return ['status' => false, 'data' => null, 'error' => $model->getErrors()];
        }
    }

    public function actionUpdate($id) {

        $model = Employee::findOne($id);
        $model->username = Yii::$app->request->post('username');
        $model->department_id = Yii::$app->request->post('department_id');


        if ($model->save()) {

            if (!empty(Yii::$app->request->post('contact'))) {
                
                Contact::deleteAll(['employee_id' => $id]);
                
                $contacts = Yii::$app->request->post('contact');
                foreach ($contacts as $contactNo) {
                    $contactModel = new Contact();
                    $contactModel->contact_no = $contactNo;
                    $contactModel->created_by = Yii::$app->user->identity->id;
                    $contactModel->employee_id = $model->id;
                    $contactModel->save();
                }
            }
            if (!empty(Yii::$app->request->post('address'))) {
                
                Address::deleteAll(['employee_id' => $id]);
                
                $addresses = Yii::$app->request->post('address');
                foreach ($addresses as $address) {
                    $addressModel = new Address();
                    $addressModel->address = $address;
                    $addressModel->employee_id = $model->id;
                    $addressModel->created_by = Yii::$app->user->identity->id;
                    $addressModel->save();
                }
            }

            return [
                'status' => true,
                'data' => $model
            ];
        } else {
            return ['status' => false, 'data' => null, 'error' => $model->getErrors()];
        }
    }
    

}

?>