<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * JvP3PpYw3P7khMCTVMbw8eqfVbKCwDwJ
 */

namespace api\controllers;

use Yii;
use yii\rest\ActiveController;
use common\models\LoginForm;


class LoginController extends ActiveController {

    public $modelClass = 'common\models\User';

    public function behaviors() {
        $behaviors = parent::behaviors();
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
        ];
        // here’s where I add behavior (read below)
        return $behaviors;
    }

    public function actionLogin() {

        if (Yii::$app->request->post()) {

            $model = new LoginForm();
            $model->username = Yii::$app->request->post('username');
            $model->password = Yii::$app->request->post('password');
            if ($model->login()) {
                return ['status' => true, 'data' => Yii::$app->user->identity];
            } else {
                return ['status' => false, 'data' => null, 'error' => $model->getErrors()];
            }
        }
    }

}

?>